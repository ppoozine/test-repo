from sqlalchemy import DATE, DATETIME, INTEGER, Column, Index, String, text, BOOLEAN
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class StripeCustomerInfo(Base):
    __tablename__ = "stripe_customer_info"
    __table_args__ = (
        Index("customer_id", "customer_id", unique=False),
    )

    user_id = Column(String(100, "utf8mb4_unicode_ci"), primary_key=True, comment="Growin 客戶編號")
    customer_id = Column(String(100, "utf8mb4_unicode_ci"), nullable=False, comment="Stripe 客戶編號")
    name = Column(String(500, "utf8mb4_unicode_ci"), nullable=True, comment="客戶姓名")
    email = Column(String(500, "utf8mb4_unicode_ci"), nullable=True, comment="電子信箱")
    phone = Column(String(20, "utf8mb4_unicode_ci"), nullable=True, comment="手機號碼")
    active = Column(String(20, "utf8mb4_unicode_ci"), nullable=False, comment="狀態")
    use_trial = Column(BOOLEAN, nullable=False, server_default="t", comment="是否試用過")
    trial_start_at = Column(DATE, nullable=False, comment="開始試用日期")
    trial_end_at = Column(DATE, nullable=False, comment="結束試用日期")
    plan = Column(String(50, "utf8mb4_unicode_ci"), nullable=False, comment="方案")
    plan_zh = Column(String(20, "utf8mb4_unicode_ci"), nullable=False, comment="方案中文")
    price_id = Column(String(100, "utf8mb4_unicode_ci"), nullable=False, comment="Stripe 產品編號")
    exp_month = Column(INTEGER, nullable=True, comment="信用卡有效月份")
    exp_year = Column(INTEGER, nullable=True, comment="信用卡有效年份")
    last_four = Column(String(4, "utf8mb4_unicode_ci"), nullable=True, comment="信用卡後四碼")
    subscribed_at = Column(DATE, nullable=True, comment="訂閱日期")
    expire_at = Column(DATE, nullable=True, comment="到期日期")
    updated_at = Column(DATETIME, nullable=True, server_default=text(
        'NULL ON UPDATE CURRENT_TIMESTAMP'), comment="資料更新時間")
    created_at = Column(DATETIME, nullable=False, server_default=text("CURRENT_TIMESTAMP"), comment="資料建立時間")
