"""create table stock_mining_service.

Revision ID: 77e750cc11fd
Revises: e75533cc3c74
Create Date: 2022-10-13 10:15:37.112385

"""
from alembic import op
import sqlalchemy as sa
import logging


# revision identifiers, used by Alembic.
revision = '77e750cc11fd'
down_revision = 'e75533cc3c74'
branch_labels = None
depends_on = None
logger = logging.getLogger("alembic")


def upgrade() -> None:
    logger.info("Create table stock_mining_service.")
    stm = """
        CREATE TABLE stock_mining_service (
            service_status VARCHAR(100)  PRIMARY KEY NOT NULL COMMENT '服務',
            is_free_open BOOLEAN NOT NULL COMMENT '是否免費開放, True: 免費開放, False: 需訂閱才能使用',
            open_at DATE NULL COMMENT '開放時間, is_free_open 為 True: YYYY-MM-DD, False: Null',
            close_at DATE NULL COMMENT '結束時間, is_free_open 為 True: YYYY-MM-DD, False: Null',
            updated_at DATETIME NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '資料異動時間，當update資料時自動更新 CURRENT_TIMESTAMP',
            created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '資料建立時間，當資料建立時自動設為 CURRENT_TIMESTAMP'
        )
    """
    op.execute(stm)


def downgrade() -> None:
    logger.info("Drop Table stock_mining_service.")
    op.execute("""DROP TABLE stock_mining_service;""")

