"""table symbol_info adjust column can null

Revision ID: 00b30042a244
Revises: 79c1b17f7910
Create Date: 2022-10-15 14:39:32.815613

"""
from alembic import op
import sqlalchemy as sa
import logging


# revision identifiers, used by Alembic.
revision = '00b30042a244'
down_revision = '79c1b17f7910'
branch_labels = None
depends_on = None
logger = logging.getLogger("alembic")


def upgrade() -> None:
    logger.info("Table symbol_info adjust column main_category_id and sub_category_id")
    op.execute("""ALTER TABLE symbol_info MODIFY COLUMN main_category_id VARCHAR(50) NULL COMMENT '大分類編號';""")
    op.execute("""ALTER TABLE symbol_info MODIFY COLUMN sub_category_id VARCHAR(50) NULL COMMENT '子分類編號';""")


def downgrade() -> None:
    logger.info("Table symbol_info adjust column main_category_id and sub_category_id")
    op.execute("""ALTER TABLE symbol_info MODIFY COLUMN main_category_id VARCHAR(50) NOT NULL COMMENT '大分類編號';""")
    op.execute("""ALTER TABLE symbol_info MODIFY COLUMN sub_category_id VARCHAR(50) NOT NULL COMMENT '子分類編號';""")
