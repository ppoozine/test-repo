"""clean alembic

Revision ID: d1422e8ca4e1
Revises: 211b1cf4f5d4
Create Date: 2022-10-12 17:45:07.830974

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd1422e8ca4e1'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
