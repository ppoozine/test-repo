"""table-stock-mining-service_status-add-column-activity

Revision ID: 79c1b17f7910
Revises: 64e62762c6ae
Create Date: 2022-10-15 01:15:54.381905

"""
from alembic import op
import sqlalchemy as sa
import logging

# revision identifiers, used by Alembic.
revision = '79c1b17f7910'
down_revision = '64e62762c6ae'
branch_labels = None
depends_on = None
logger = logging.getLogger("alembic")


def upgrade() -> None:
    logger.info("Table stock_mining_service add column activity.")
    op.execute("""ALTER TABLE stock_mining_service ADD COLUMN activity VARCHAR(20) NOT NULL COMMENT '活動' AFTER service_status;""")


def downgrade() -> None:
    logger.info("Table stock_mining_service drop column activity.")
    op.execute("""ALTER TABLE stock_mining_service DROP COLUMN activity;""")
