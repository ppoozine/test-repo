"""table famous_investor_status add column created_at

Revision ID: 64e62762c6ae
Revises: 77e750cc11fd
Create Date: 2022-10-14 18:04:35.111185

"""
from alembic import op
import sqlalchemy as sa
import logging


# revision identifiers, used by Alembic.
revision = '64e62762c6ae'
down_revision = '77e750cc11fd'
branch_labels = None
depends_on = None
logger = logging.getLogger("alembic")


def upgrade() -> None:
    logger.info("Table famous_investor_status add Column updated_at.")
    op.execute("""ALTER TABLE famous_investor_status ADD COLUMN updated_at DATETIME NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '資料異動時間，當update資料時自動更新 CURRENT_TIMESTAMP' after units_pct_4;""")


def downgrade() -> None:
    logger.info("Table famous_investor_status Drop Column updated_at.")
    op.execute("""ALTER TABLE famous_investor_status DROP COLUMN updated_at;""")
