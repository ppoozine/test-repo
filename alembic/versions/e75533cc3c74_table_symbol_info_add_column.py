"""table symbol_info add column

Revision ID: e75533cc3c74
Revises: d1422e8ca4e1
Create Date: 2022-10-12 18:00:34.347975

"""
from alembic import op
import sqlalchemy as sa
import logging


# revision identifiers, used by Alembic.
revision = 'e75533cc3c74'
down_revision = 'd1422e8ca4e1'
branch_labels = None
depends_on = None
logger = logging.getLogger("alembic")


def upgrade() -> None:
    logger.info("Table symbol_info add exchange.")
    op.execute("""ALTER TABLE symbol_info ADD COLUMN exchange varchar(10) NULL COMMENT '交易所' after name;""")


def downgrade() -> None:
    logger.info("Table symbol_info remove exchange.")
    op.execute("""ALTER TABLE symbol_info DROP COLUMN exchange;""")
