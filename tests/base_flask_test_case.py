import os
import re
from unittest import TestCase

from flask import Flask
from flask.ctx import AppContext
from flask_caching.backends.base import BaseCache
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker

from api import create_app
from api.containers.services_container import ServicesContainer
from config.api_config import Config

TestCase.maxDiff = None


class BaseFlaskTestCase(TestCase):
    app: Flask
    app_context: AppContext
    container: ServicesContainer
    nike_sql_session: Session
    symbol_sql_session: Session
    cache: BaseCache

    def __init__(self, *args, **kargs) -> None:
        super().__init__(*args, **kargs)
        self.tests_path = os.path.realpath(os.path.dirname(__file__))
        self.fixtures_path = os.path.join(self.tests_path, "file_fixtures")

    @classmethod
    def set_mock_jwt(cls, mock_user_id, mock_exp, email, account_type):
        cls.mock_jwt = {"user_id": mock_user_id, "exp": mock_exp, "email": email, "account_type": account_type}
        return cls.mock_jwt

    @classmethod
    def setUpClass(cls) -> None:
        import flask_jwt_extended

        cls.old_verify_jwt = flask_jwt_extended.verify_jwt_in_request

        def mock_verify_jwt():
            pass

        flask_jwt_extended.verify_jwt_in_request = mock_verify_jwt

        cls.old_jwt = flask_jwt_extended.get_jwt

        def mock_get_jwt():
            return cls.mock_jwt

        flask_jwt_extended.get_jwt = mock_get_jwt

        super().setUpClass()
        cls.app: Flask = create_app()
        cls.app.testing = True
        cls.app_context = cls.app.app_context()
        cls.app_context.push()
        cls.app.logger.setLevel("ERROR")
        cls.container = cls.app.extensions["container"]
        cls.cache: BaseCache = cls.container.provide("cache")
        cls.cache.clear()

        db_host_list = [Config.NIKE_DB_HOST, Config.SYMBOL_DB_HOST, Config.SEC13F_DB_HOST]
        for db_host in db_host_list:
            if db_host and not re.search(r'^\s*localhost\s*$|^\s*127.0.0.1\s*$', db_host):
                raise Exception('測試請使用本地端的資料庫')

        nike_engine = create_engine(
            cls.app.config["NIKE_DATABASE_URI"],
            pool_size=cls.app.config["SQLALCHEMY_POOL_SIZE"],
            pool_recycle=cls.app.config["SQLALCHEMY_POOL_RECYCLE"],
            pool_timeout=cls.app.config["SQLALCHEMY_POOL_TIMEOUT"],
            encoding="utf8",
            pool_pre_ping=True
        )

        cls.nike_sql_session = sessionmaker(bind=nike_engine, autocommit=True)()

        symbol_engine = create_engine(
            cls.app.config["SYMBOL_DATABASE_URI"],
            pool_size=cls.app.config["SQLALCHEMY_POOL_SIZE"],
            pool_recycle=cls.app.config["SQLALCHEMY_POOL_RECYCLE"],
            pool_timeout=cls.app.config["SQLALCHEMY_POOL_TIMEOUT"],
            encoding="utf8",
            pool_pre_ping=True
        )

        cls.symbol_sql_session = sessionmaker(bind=symbol_engine, autocommit=True)()

        sec13f_engine = create_engine(
            cls.app.config["SEC13F_DATABASE_URI"],
            pool_size=cls.app.config["SQLALCHEMY_POOL_SIZE"],
            pool_recycle=cls.app.config["SQLALCHEMY_POOL_RECYCLE"],
            pool_timeout=cls.app.config["SQLALCHEMY_POOL_TIMEOUT"],
            encoding="utf8",
            pool_pre_ping=True
        )

        cls.sec13f_sql_session = sessionmaker(bind=sec13f_engine, autocommit=True)()

    @classmethod
    def tearDownClass(cls) -> None:
        if cls.nike_sql_session:
            cls.nike_sql_session.close_all()

        if cls.symbol_sql_session:
            cls.symbol_sql_session.close_all()

        if cls.sec13f_sql_session:
            cls.sec13f_sql_session.close_all()

        setattr(cls, "app", None)
        setattr(cls, "app_context", None)
        setattr(cls, "container", None)
        setattr(cls, 'nike_sql_session', None)
        setattr(cls, 'symbol_sql_session', None)
        setattr(cls, 'cache', None)

        import flask_jwt_extended
        flask_jwt_extended.verify_jwt_in_request = cls.old_verify_jwt
        flask_jwt_extended.get_jwt = cls.old_jwt
        super().tearDownClass()

    def setUp(self) -> None:
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()
