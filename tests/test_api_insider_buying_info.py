from requests import Response

from dbmodels.nike.insider_buying_info import InsiderBuyingInfo
from tests.base_flask_test_case import BaseFlaskTestCase


class TestApiInsiderBuyingInfo(BaseFlaskTestCase):

    def setUp(self) -> None:
        super().setUp()
        self.set_mock_jwt("set-account-id", 1000, "test@email.com", "normal")
        self._clear_test_data()
        self._prepare_test_data()

    def tearDown(self) -> None:
        self._clear_test_data()
        return super().tearDown()

    def _prepare_test_data(self):
        for i in range(15):
            self.nike_sql_session.add(InsiderBuyingInfo(**{
                "symbol": "DUMMY",
                "filing_date": f"2022-06-{str(i).zfill(2)}",
            }))
        self.nike_sql_session.flush()

    def _clear_test_data(self):
        self.nike_sql_session.query(InsiderBuyingInfo).filter(InsiderBuyingInfo.symbol == "DUMMY").delete()
        self.nike_sql_session.flush()

    def test_it_should_404_when_url_is_wrong(self):
        """URL錯誤，應該回應404"""
        with self.app.test_client() as client:
            res: Response = client.get('/insider-buying')
            self.assertEqual(404, res.status_code)

    def test_it_should_200_when_symbol_not_found(self):
        """找不到Symbol，但會回應200"""
        with self.app.test_client() as client:
            res: Response = client.get("/insider-buying-info/SAMPLE/1/10")
            self.assertEqual(200, res.status_code)
            self.assertEqual([], res.json["data"])

    def test_it_should_200_or_404_when_parameter_invalid(self):
        """Payload參數錯誤，應該回應422"""
        with self.app.test_client() as client:
            res: Response = client.get("/insider-buying-info//1/10")
            self.assertEqual(404, res.status_code)

            res: Response = client.get("/insider-buying-info/1/1/10")
            self.assertEqual(200, res.status_code)
            self.assertEqual([], res.json["data"])

    def test_it_should_200_when_symbol_exsits_and_page_1(self):
        """Symbol存在並且取第一頁資訊，應該回應200"""
        with self.app.test_client() as client:
            res: Response = client.get("/insider-buying-info/DUMMY/1/10")
            print(res.json)
            self.assertEqual(200, res.status_code)
            self.assertEqual(10, len(res.json["data"]))

    def test_it_should_200_when_symbol_exsits_and_page_2(self):
        """Symbol存在並且取第一頁資訊，應該回應200"""
        with self.app.test_client() as client:
            res: Response = client.get("/insider-buying-info/DUMMY/2/10")
            self.assertEqual(200, res.status_code)
            self.assertEqual(5, len(res.json["data"]))
