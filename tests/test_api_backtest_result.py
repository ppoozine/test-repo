from requests import Response

from dbmodels.nike.backtest_result import BacktestResult
from dbmodels.nike.symbol_info import SymbolInfo
from tests.base_flask_test_case import BaseFlaskTestCase


class TestApiBacktestResult(BaseFlaskTestCase):

    def setUp(self) -> None:
        super().setUp()
        self.set_mock_jwt("set-account-id", 1000, "test@email.com", "normal")
        self._clear_test_data()
        self._prepare_test_data()

    def tearDown(self) -> None:
        self._clear_test_data()
        return super().tearDown()

    def _prepare_test_data(self):
        self.nike_sql_session.add(SymbolInfo(**{
            "symbol": "DUMMY",
            "name": "DUMMY",
            "country": "US",
            "price": 100.12,
            "main_category": "main",
            "main_category_id": "mian",
            "sub_category": "sub",
            "sub_category_id": "sub"
        }))
        self.nike_sql_session.add_all([
            BacktestResult(**{
                "symbol": "DUMMY",
                "oriented": "value",
                "version": "1.0",
                "strategy": "4_3",
                "occurrence": 2,
                "profits": 1,
                "profitability": 50.0,
                "avg_profit_and_loss": 12.1,
                "avg_profit": 12.1,
                "avg_loss": 12.1,
                "avg_profit_to_loss_ratio": 12.1,
                "quantile_25": 12.1,
                "quantile_50": 12.1,
                "quantile_75": 12.1,
                "avg_holding_time": 12.1
            }),
            BacktestResult(**{
                "symbol": "DUMMY",
                "oriented": "value",
                "version": "2.0",
                "strategy": "4_3",
                "occurrence": 2,
                "profits": 1,
                "profitability": 50.0,
                "avg_profit_and_loss": 12.1,
                "avg_profit": 12.1,
                "avg_loss": 12.1,
                "avg_profit_to_loss_ratio": 12.1,
                "quantile_25": 12.1,
                "quantile_50": 12.1,
                "quantile_75": 12.1,
                "avg_holding_time": 12.1
            }),
            BacktestResult(**{
                "symbol": "main",
                "oriented": "value",
                "version": "1.0",
                "strategy": "4_3",
                "occurrence": 2,
                "profits": 1,
                "profitability": 50.0,
                "avg_profit_and_loss": 12.1,
                "avg_profit": 12.1,
                "avg_loss": 12.1,
                "avg_profit_to_loss_ratio": 12.1,
                "quantile_25": 12.1,
                "quantile_50": 12.1,
                "quantile_75": 12.1,
                "avg_holding_time": 12.1
            }),
            BacktestResult(**{
                "symbol": "DUMMY",
                "oriented": "swing",
                "version": "1.0",
                "strategy": "4_3",
                "occurrence": 2,
                "profits": 1,
                "profitability": 50.0,
                "avg_profit_and_loss": 12.1,
                "avg_profit": 12.1,
                "avg_loss": 12.1,
                "avg_profit_to_loss_ratio": 12.1,
                "quantile_25": 12.1,
                "quantile_50": 12.1,
                "quantile_75": 12.1,
                "avg_holding_time": 12.1
            }),
            BacktestResult(**{
                "symbol": "main",
                "oriented": "swing",
                "version": "1.0",
                "strategy": "4_3",
                "occurrence": 2,
                "profits": 1,
                "profitability": 50.0,
                "avg_profit_and_loss": 12.1,
                "avg_profit": 12.1,
                "avg_loss": 12.1,
                "avg_profit_to_loss_ratio": 12.1,
                "quantile_25": 12.1,
                "quantile_50": 12.1,
                "quantile_75": 12.1,
                "avg_holding_time": 12.1
            }),
        ])
        self.nike_sql_session.flush()

        self.symbol_sql_session.execute("""
                    INSERT INTO history_bar(symbol, time, time_frame) VALUES
                    ('DUMMY', '2007-01-01', 'D'),
                    ('DUMMY', '2007-01-02', 'D'),
                    ('DUMMY', '2007-01-03', 'D');
                """)
        self.symbol_sql_session.flush()

    def _clear_test_data(self):
        self.nike_sql_session.query(SymbolInfo).filter(SymbolInfo.symbol == "DUMMY").delete()
        self.nike_sql_session.query(BacktestResult).filter(BacktestResult.symbol.in_(["DUMMY", "main"])).delete()
        self.nike_sql_session.flush()
        self.symbol_sql_session.execute("""DELETE FROM history_bar WHERE symbol = 'DUMMY';""")
        self.symbol_sql_session.flush()

    def test_it_should_404_when_url_is_wrong(self):
        """URL錯誤，應該回應404"""
        with self.app.test_client() as client:
            res: Response = client.get('/backtest-resul')
            self.assertEqual(404, res.status_code)

    def test_it_should_200_when_symbol_not_found(self):
        """找不到Symbol，應該回應404"""
        with self.app.test_client() as client:
            res: Response = client.get("/backtest-result/SAMPLE/value/4_3", )
            self.assertEqual(404, res.status_code)

    def test_it_should_404_when_parameter_invalid(self):
        """path參數錯誤，應該回應404"""
        with self.app.test_client() as client:
            res: Response = client.get("/backtest-result/1/value/4_3")
            self.assertEqual(404, res.status_code)

            res: Response = client.get("/backtest-result//value/4_3")
            self.assertEqual(404, res.status_code)

            res: Response = client.get("/backtest-result/DUMMY/1/4_3")
            self.assertEqual(400, res.status_code)

    def test_it_should_200_when_symbol_exsits(self):
        """path正確且symbol存在，應該回應200"""
        with self.app.test_client() as client:
            res: Response = client.get("/backtest-result/DUMMY/value/4_3")
            self.assertEqual(200, res.status_code)
            self.assertIsNotNone(res.json["data"])

            res: Response = client.get("/backtest-result/DUMMY/swing/4_3")
            self.assertEqual(200, res.status_code)
            self.assertIsNotNone(res.json["data"])
