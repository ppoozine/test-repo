from requests import Response

from dbmodels.nike.reward_of_trend_period import RewardOfTrendPeriod
from tests.base_flask_test_case import BaseFlaskTestCase


class TestApiTrendPerformance(BaseFlaskTestCase):

    def setUp(self) -> None:
        super().setUp()
        self.set_mock_jwt("set-account-id", 1000, "test@email.com", "normal")
        self._clear_test_data()
        self._prepare_test_data()

    def tearDown(self) -> None:
        self._clear_test_data()
        return super().tearDown()

    def _prepare_test_data(self):
        self.nike_sql_session.add_all([
            RewardOfTrendPeriod(**{
                "symbol": "DUMMY",
                "score": 1,
                "occurrence": 10,
                "profits": 5,
                "profitability": 45
            }),
            RewardOfTrendPeriod(**{
                "symbol": "DUMMY",
                "score": 2,
                "occurrence": 10,
                "profits": 5,
                "profitability": 45
            }),
            RewardOfTrendPeriod(**{
                "symbol": "DUMMY",
                "score": 3,
                "occurrence": 10,
                "profits": 5,
                "profitability": 45
            }),
            RewardOfTrendPeriod(**{
                "symbol": "DUMMY",
                "score": 4,
                "occurrence": 10,
                "profits": 5,
                "profitability": 45
            }),
            RewardOfTrendPeriod(**{
                "symbol": "DUMMY",
                "score": 5,
                "occurrence": 10,
                "profits": 5,
                "profitability": 45
            }),
        ])
        self.nike_sql_session.flush()

        self.symbol_sql_session.execute("""
            INSERT INTO history_bar(symbol, time, time_frame) VALUES
            ('DUMMY', '2007-01-01', 'D'),
            ('DUMMY', '2007-01-02', 'D'),
            ('DUMMY', '2007-01-03', 'D');
        """)
        self.symbol_sql_session.flush()

    def _clear_test_data(self):
        self.nike_sql_session.query(RewardOfTrendPeriod).filter(RewardOfTrendPeriod.symbol == "DUMMY").delete()
        self.nike_sql_session.flush()
        self.symbol_sql_session.execute("""DELETE FROM history_bar WHERE symbol = 'DUMMY';""")
        self.symbol_sql_session.flush()

    def test_it_should_404_when_url_is_wrong(self):
        """URL錯誤，應該回應404"""
        with self.app.test_client() as client:
            res: Response = client.get('/trend-performane/DUMMY')
            self.assertEqual(404, res.status_code)

    def test_it_should_200_when_symbol_not_found(self):
        """找不到Symbol，應該回應200"""
        with self.app.test_client() as client:
            res: Response = client.get("/trend-performance/SAMPLE")
            self.assertEqual(200, res.status_code)
            self.assertEqual([], res.json["data"]["scoreList"])
            self.assertEqual(None, res.json["data"]["updatedAt"])

    def test_it_should_200_when_symbol_is_exists(self):
        """Symbol存在，應該回應200"""
        with self.app.test_client() as client:
            res: Response = client.get("/trend-performance/DUMMY")
            self.assertEqual(200, res.status_code)
