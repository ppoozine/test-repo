from datetime import datetime

from requests import Response

from dbmodels.nike.famous_investor_status import FamousInvestorStatus
from tests.base_flask_test_case import BaseFlaskTestCase


class TestApiInvestorRanking(BaseFlaskTestCase):

    def setUp(self) -> None:
        super().setUp()
        self.set_mock_jwt("set-account-id", 1000, "test@email.com", "normal")
        self._clear_test_data()
        self._prepare_test_data()

    def tearDown(self) -> None:
        self._clear_test_data()
        return super().tearDown()

    def _prepare_test_data(self):
        self.nike_sql_session.add(FamousInvestorStatus(**{
            "symbol": "DUMMY",
            "investorname": "DUMMY-investor",
            "investorname_zh": "DUMMY-investor-zh",
            "quarter_1": "2022-06-30",
            "units_1": "-",
            "units_pct_1": "清倉",
            "quarter_2": "2022-03-31",
            "units_2": "128959.0",
            "units_pct_2": "建倉",
            "quarter_3": "2021-12-31",
            "units_3": "-",
            "units_pct_3": "-",
            "quarter_4": "2021-09-30",
            "units_4": "-",
            "units_pct_4": "清倉",
        }))
        self.nike_sql_session.flush()

    def _clear_test_data(self):
        self.nike_sql_session.query(FamousInvestorStatus).filter(FamousInvestorStatus.symbol == "DUMMY").delete()
        self.nike_sql_session.flush()

    def test_it_should_404_when_url_is_wrong(self):
        """URL錯誤，應該回應404"""
        with self.app.test_client() as client:
            res: Response = client.get('/famous-investor-statuss/TSLA')
            self.assertEqual(404, res.status_code)

    def test_it_should_200_when_symbol_not_exsits(self):
        """Sybmol不存在，應該回應200"""
        with self.app.test_client() as client:
            res: Response = client.get('/famous-investor-status/TSLA')
            self.assertEqual(200, res.status_code)
            self.assertEqual([], res.json["fourQuarters"])
            self.assertEqual(datetime.now().strftime("%Y-%m-%d"), res.json["updatedAt"])
            self.assertEqual([], res.json["data"])

    def test_it_should_200_when_symbol_is_exsits(self):
        """Symbol存在，應該回應200"""
        with self.app.test_client() as client:
            res: Response = client.get('/famous-investor-status/DUMMY')
            self.assertEqual(200, res.status_code)
            self.assertEqual(["2022 Q2", "2022 Q1", "2021 Q4", "2021 Q3"], res.json["fourQuarters"])
            self.assertEqual(datetime.now().strftime("%Y-%m-%d"), res.json["updatedAt"])
            self.assertEqual([
                [
                    {
                        "investorname": "DUMMY-investor",
                        "investornameZh": "DUMMY-investor-zh"
                    },
                    {
                        "units": "-",
                        "unitsPct": "清倉"
                    },
                    {
                        "units": 128959.0,
                        "unitsPct": "建倉"
                    },
                    {
                        "units": "-",
                        "unitsPct": "-"
                    },
                    {
                        "units": "-",
                        "unitsPct": "清倉"
                    }
                ]
            ], res.json["data"])
