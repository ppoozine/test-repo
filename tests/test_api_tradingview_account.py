from unittest.mock import patch

from requests import Response

from api.biz.account.tradingview_account_service import TradingviewAccountService
from dbmodels.nike.tradingview_account_list import TradingviewAccountList
from tests.base_flask_test_case import BaseFlaskTestCase


class TestApiTradingviewAccount(BaseFlaskTestCase):

    def setUp(self) -> None:
        super().setUp()
        self.set_mock_jwt("set-account-id", 1000, "test@email.com", "normal")
        self._clear_test_data()
        self._prepare_test_data()

    def tearDown(self) -> None:
        self._clear_test_data()
        return super().tearDown()

    def _prepare_test_data(self):
        return

    def _clear_test_data(self):
        self.nike_sql_session.execute("""TRUNCATE TABLE tradingview_account_list;""")
        self.nike_sql_session.flush()

    def test_it_should_404_when_url_is_wrong(self):
        """URL錯誤，應該回應404"""
        with self.app.test_client() as client:
            res: Response = client.post('/tradingview/accounts', json={})
            self.assertEqual(404, res.status_code)

    def test_it_should_422_when_payload_not_correct(self):
        """Payload不正確，應該回應422"""
        with self.app.test_client() as client:
            payload = {
                "tradingviewAccounts": "account"
            }
            res: Response = client.post('/tradingview/account', json=payload)
            self.assertEqual(422, res.status_code)

    def test_it_should_200_when_db_not_data_and_update_data(self):
        """DB沒有資料，可以成功寫入，應該回應200"""
        with patch.object(TradingviewAccountService, '_discord_hook', return_value=None) as mocked_discord:
            with self.app.test_client() as client:
                payload = {"tradingviewAccount": "tradingview-id-1"}
                res: Response = client.post('/tradingview/account', json=payload)
                self.assertEqual(200, res.status_code)

                account: TradingviewAccountList = self.nike_sql_session.query(TradingviewAccountList) \
                    .filter(TradingviewAccountList.user_id == "set-account-id") \
                    .first()

                self.assertEqual("set-account-id", account.user_id)
                self.assertEqual(payload["tradingviewAccount"], account.tradingview_account)
                self.assertEqual(None, account.last_tradingview_account)
            mocked_discord.assert_called()

    def test_it_should_200_when_update_data(self):
        """DB有資料，可以成功更新，應該回應200"""
        with patch.object(TradingviewAccountService, '_discord_hook', return_value=None) as mocked_discord:
            with self.app.test_client() as client:
                self._clear_test_data()
                self.nike_sql_session.add(TradingviewAccountList(**{
                    "user_id": "set-account-id",
                    "tradingview_account": "tradingview-id-1"
                }))
                self.nike_sql_session.flush()

                payload = {"tradingviewAccount": "tradingview-id-2"}
                res: Response = client.post('/tradingview/account', json=payload)
                self.assertEqual(200, res.status_code)

                account: TradingviewAccountList = self.nike_sql_session.query(TradingviewAccountList) \
                    .filter(TradingviewAccountList.user_id == "set-account-id") \
                    .first()

                self.assertEqual("set-account-id", account.user_id)
                self.assertEqual(payload["tradingviewAccount"], account.tradingview_account)
                self.assertEqual("tradingview-id-1", account.last_tradingview_account)
            mocked_discord.assert_called()
