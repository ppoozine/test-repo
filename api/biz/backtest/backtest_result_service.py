from logging import Logger

from flask_caching import Cache
from sqlalchemy.orm import Session

from api.biz.error import DataNotFound
from api.biz.symbol.symbol_service import SymbolService
from api.biz.symbol.symbol_info_service import SymbolInfoService
from api.common.datetime_utils import date_to_str
from api.common.number_utils import decimal_to_float
from api.common.updated_at_utils import UpdatedAtUtils
from dbmodels.nike.backtest_result import BacktestResult
from dbmodels.nike.symbol_info import SymbolInfo


class BacktestResultService:
    def __init__(
            self,
            cache: Cache,
            logger: Logger,
            nike_sql_session: Session,
            symbol_info_service: SymbolInfoService,
            symbol_service: SymbolService,
            updated_at_utils: UpdatedAtUtils,
    ):
        self._cache = cache
        self._logger = logger
        self._nike_sql_session = nike_sql_session
        self._symbol_info_service = symbol_info_service
        self._symbol_service = symbol_service
        self._updated_at_utils = updated_at_utils

    def get_backtest_result(self, symbol: str, oriented: str, strategy: str) -> BacktestResult:
        return self._nike_sql_session.query(BacktestResult) \
            .filter(BacktestResult.symbol == symbol) \
            .filter(BacktestResult.oriented == oriented) \
            .filter(BacktestResult.strategy == strategy) \
            .order_by(BacktestResult.version.desc()) \
            .first()

    def backtest_result(self, symbol: str, oriented: str, strategy: str) -> dict:
        cache_key = self._backtest_result_cache_key(symbol, oriented, strategy)
        cached = self._cache.get(cache_key)
        if cached:
            return cached

        symbol_info: SymbolInfo = self._symbol_info_service.get_symbol_info_by_symbol(symbol)
        if not symbol_info:
            raise DataNotFound(f"找不到指定的Symbol -> {symbol}")

        updated_at = self._updated_at_utils.get_date("nike", "backtest_result")
        start_at, end_at = self._symbol_service.symbol_price_start_date_and_end_date(symbol_info.symbol)

        data = {
            "updatedAt": updated_at,
            "startAt": date_to_str(start_at, "%Y/%m"),
            "endAt": date_to_str(end_at, "%Y/%m")
        }

        column = self._show_column(symbol_info, oriented)
        for c in column:
            result: BacktestResult = self.get_backtest_result(column[c], oriented, strategy)
            if not result:
                data[c] = {
                    "occurrence": None,
                    "profits": None,
                    "losses": None,
                    "profitability": None,
                    "avgHoldingTime": None,
                    "avgProfitAndLoss": None,
                    "avgProfit": None,
                    "avgLoss": None,
                    "avgProfitToLossRatio": None,
                    "quantile25": None,
                    "quantile50": None,
                    "quantile75": None,
                }
                continue

            data[c] = {
                "occurrence": result.occurrence,
                "profits": result.profits,
                "losses": result.occurrence - result.profits,
                "profitability": decimal_to_float(result.profitability),
                "avgHoldingTime": decimal_to_float(result.avg_holding_time),
                "avgProfitAndLoss": decimal_to_float(result.avg_profit_and_loss),
                "avgProfit": decimal_to_float(result.avg_profit),
                "avgLoss": decimal_to_float(result.avg_loss),
                "avgProfitToLossRatio": decimal_to_float(result.avg_profit_to_loss_ratio),
                "quantile25": decimal_to_float(result.quantile_25),
                "quantile50": decimal_to_float(result.quantile_50),
                "quantile75": decimal_to_float(result.quantile_75),
            }

        self._cache.set(cache_key, data, timeout=3600)
        return data

    @staticmethod
    def _backtest_result_cache_key(symbol: str, oriented: str, strategy: str) -> str:
        return f"{symbol}_{oriented}_{strategy}_backtest_result"

    @staticmethod
    def _show_column(symbol_info: SymbolInfo, oriented: str) -> dict:
        if oriented == "swing":
            return {
                "symbol": symbol_info.symbol,
                "sameIndustry": symbol_info.main_category,
                "S&P500": "S&P500",
                "allMarket": "ALL_MARKET"
            }
        if oriented == "value":
            return {
                "symbol": symbol_info.symbol,
                "sameIndustry": f"{symbol_info.symbol}_sameIndustry",
                "S&P500": f"{symbol_info.symbol}_S&P500",
                "allMarket": f"{symbol_info.symbol}_ALL_MARKET"
            }
        return {
            "symbol": "",
            "sameIndustry": "",
            "S&P500": "",
            "allMarket": ""
        }
