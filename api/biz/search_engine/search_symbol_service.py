from typing import List

from sqlalchemy.orm import Session

from dbmodels.nike.symbol_info import SymbolInfo


class SearchSymbolService:
    def __init__(self, nike_sql_session: Session) -> None:
        self._nike_sql_session = nike_sql_session

    def search(self, keyword: str) -> List[str]:
        results: List[SymbolInfo] = self._nike_sql_session.query(SymbolInfo) \
            .filter(SymbolInfo.symbol.like(f"{keyword}%")) \
            .order_by(SymbolInfo.symbol) \
            .limit(8) \
            .all()

        data = []
        for r in results:
            data.append({
                "symbol": r.symbol,
                "name": r.name,
                "countryCode": r.country,
                "mainCategory": r.main_category,
                "mainCategoryZhTw": r.main_category_zh_tw
            })

        return data

    def default_search(self) -> List[str]:
        symbol_list = ["AAPL", "TSLA", "NVDA", "AMZN", "TSM", "ABNB", "SBUX", "COST"]

        results: List[SymbolInfo] = self._nike_sql_session.query(SymbolInfo) \
            .filter(SymbolInfo.symbol.in_(symbol_list)) \
            .order_by(SymbolInfo.symbol) \
            .all()

        data = []
        for r in results:
            data.append({
                "symbol": r.symbol,
                "name": r.name,
                "countryCode": r.country,
                "mainCategory": r.main_category,
                "mainCategoryZhTw": r.main_category_zh_tw
            })

        return data
