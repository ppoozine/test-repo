SCREENER_SORT_COLUMN = [
    "symbol",
    "marketCap",
    "volume",
    "dividend",
    "trend",
    "chip",
    "swing",
    "value",
    "powerSqueezeDaily",
    "powerSqueezeWeekly",
    "surfingTrendDaily",
    "surfingTrendWeekly"
]
