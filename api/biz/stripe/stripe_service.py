from logging import Logger

import stripe
from sqlalchemy.orm import Session

from api.biz.stripe.stripe_customer_info_service import StripeCustomerInfoService
from api.biz.stripe.stripe_subscribe_info_service import StripeSubscribeInfoService
from config.api_config import Config
from dbmodels.nike.stripe_customer_info import StripeCustomerInfo
from dbmodels.nike.stripe_subscribe_info import StripeSubscribeInfo

stripe.api_key = Config.STRIPE_SECRET_KEY


class StripeService:
    def __init__(
            self,
            config: Config,
            logger: Logger,
            nike_sql_session: Session,
            stripe_customer_info_service: StripeCustomerInfoService,
            stripe_subscribe_info_service: StripeSubscribeInfoService
    ) -> None:
        self._config = config
        self._logger = logger
        self._nike_sql_session = nike_sql_session
        self._stripe_customer_info_service = stripe_customer_info_service
        self._stripe_subscribe_info_service = stripe_subscribe_info_service

    @staticmethod
    def stripe_customer_retrieve(customer_id: str) -> dict:
        return stripe.Customer.retrieve(customer_id)

    @staticmethod
    def stripe_payment_method_retrieve(payment_method_id: str) -> dict:
        return stripe.PaymentMethod.retrieve(payment_method_id)

    @staticmethod
    def stripe_subscription_retrieve(subscription_id: str) -> dict:
        return stripe.Subscription.retrieve(subscription_id)

    @staticmethod
    def stripe_invoice_retrieve(invoice_id: str) -> dict:
        return stripe.Invoice.retrieve(invoice_id)

    def customer_portal(self, user_id: str, return_url: str) -> dict:
        cus_info: StripeCustomerInfo = self._stripe_customer_info_service.get("user_id", user_id)

        if cus_info:
            session = stripe.billing_portal.Session.create(
                customer=cus_info.customer_id,
                return_url=return_url
            )
            return {"sessionUrl": session.url}
        return {"sessionUrl": None}

    def customer_portal_update_page(self, user_id: str, return_url: str) -> dict:
        cus_info: StripeCustomerInfo = self._stripe_customer_info_service.get("user_id", user_id)

        if cus_info:
            session = stripe.billing_portal.Session.create(
                customer=cus_info.customer_id,
                return_url=return_url
            )
            subscribe_info: StripeSubscribeInfo = self._stripe_subscribe_info_service.get(
                "customer_id", cus_info.customer_id)
            return {"sessionUrl": f"{session.url}/subscriptions/{subscribe_info.subscription_id}/update"}
        return {"sessionUrl": None}
