import uuid
from typing import Optional

from sqlalchemy.orm import Session

from api.common.datetime_utils import date_to_str
from api.common.mail.system_mailer import SystemMailer
from dbmodels.user_profile.nike_customer_info import NikeCustomerInfo


class NormalAccountService:
    def __init__(
            self,
            system_mailer: SystemMailer,
            user_sql_session: Session
    ) -> None:
        self._system_mailer = system_mailer
        self._user_sql_session = user_sql_session

    def get_user(self, user_id: str) -> Optional[dict]:
        cus_info: NikeCustomerInfo = self._user_sql_session.query(NikeCustomerInfo) \
            .filter(NikeCustomerInfo.user_id == user_id) \
            .first()

        if not cus_info:
            return None

        subscribed = False
        if cus_info.status in ["active", "trialing"]:
            subscribed = True

        return {
            "subscribed": subscribed,
            "plan": cus_info.plan,
            "subscribed_at": date_to_str(cus_info.subscribed_at),
            "subscribe_expired_at": date_to_str(cus_info.expire_at),
            "use_trial": cus_info.use_trial,
            "trial_end_at": date_to_str(cus_info.trial_end_at),
            "watchlists": cus_info.watchlist
        }

    def add_user(self, user_id: str, email: str):
        self._user_sql_session.add(NikeCustomerInfo(**{
            "user_id": user_id,
            "plan": "Free",
            "email": email,
            "status": "inactive",
            "use_trial": False,
            "watchlist": [
                {
                    "watchlist_id": str(uuid.uuid4().hex),
                    "name": "我的追蹤清單",
                    "symbols": [],
                }
            ]
        }))
        self._user_sql_session.flush()
        self._system_mailer.send_trial_plan_mail(email=email)
