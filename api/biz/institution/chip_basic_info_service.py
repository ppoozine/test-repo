from logging import Logger

from flask_caching import Cache
from sqlalchemy.orm import Session

from api.biz.symbol.symbol_service import SymbolService
from api.common.updated_at_utils import UpdatedAtUtils
from dbmodels.nike.chip_basic_info import ChipBasicInfo


class ChipBasicInfoService:
    def __init__(
            self,
            cache: Cache,
            logger: Logger,
            nike_sql_session: Session,
            symbol_service: SymbolService,
            updated_at_utils: UpdatedAtUtils
    ):
        self._cache = cache
        self._logger = logger
        self._nike_sql_session = nike_sql_session
        self._symbol_service = symbol_service
        self._updated_at_utils = updated_at_utils

    def get_chip_basic_info_by_symbol(self, symbol: str) -> ChipBasicInfo:
        return self._nike_sql_session.query(ChipBasicInfo) \
            .filter(ChipBasicInfo.symbol == symbol) \
            .order_by(ChipBasicInfo.date.desc()) \
            .first()

    def chip_basic_info(self, symbol: str) -> dict:
        cache_key = self._chip_basic_info_cache_key(symbol)
        cached = self._cache.get(cache_key)
        if cached:
            return cached

        symbol = self._symbol_service.symbol_mapping_by_symbol(symbol)
        updated_at = self._updated_at_utils.get_date("nike", "chip_basic_info")
        result: ChipBasicInfo = self.get_chip_basic_info_by_symbol(symbol)
        if not result:
            return {
                "updatedAt": None,
                "cashSurplus": None,
                "cashNetIn": None,
                "cashNetOut": None,
                "addInstNum": None,
                "addTotalCash": None,
                "buildInstNum": None,
                "buildTotalCash": None,
                "reduceInstNum": None,
                "reduceTotalCash": None,
                "cleanInstNum": None,
                "cleanTotalCash": None
            }

        data = {
            "updatedAt": updated_at,
            "cashSurplus": result.cash_surplus,
            "cashNetIn": result.cash_net_in,
            "cashNetOut": result.cash_net_out,
            "addInstNum": result.add_inst_num,
            "addTotalCash": result.add_total_cash,
            "buildInstNum": result.build_inst_num,
            "buildTotalCash": result.build_total_cash,
            "reduceInstNum": result.reduce_inst_num,
            "reduceTotalCash": result.reduce_total_cash,
            "cleanInstNum": result.clean_inst_num,
            "cleanTotalCash": result.clean_total_cash
        }
        self._cache.set(cache_key, data, timeout=3600)
        return data

    @staticmethod
    def _chip_basic_info_cache_key(symbol: str) -> str:
        return f"{symbol}_chip_basic_info"
