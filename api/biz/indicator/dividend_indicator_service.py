from flask_caching import Cache
from sqlalchemy.orm import Session

from api.biz.error import DataNotFound
from api.biz.indicator.indicator_result_service import IndicatorResultService
from api.biz.symbol.symbol_info_service import SymbolInfoService
from api.common.number_utils import decimal_to_float
from api.common.updated_at_utils import UpdatedAtUtils
from dbmodels.nike.symbol_dividend_info import SymbolDividendInfo
from dbmodels.nike.symbol_info import SymbolInfo


class DividendIndicatorService:
    def __init__(
            self,
            cache: Cache,
            indicator_result_service: IndicatorResultService,
            nike_sql_session: Session,
            symbol_info_service: SymbolInfoService,
            updated_at_utils: UpdatedAtUtils,
    ):
        self._cache = cache
        self._indicator_result_service = indicator_result_service
        self._nike_sql_session = nike_sql_session
        self._symbol_info_service = symbol_info_service
        self._updated_at_utils = updated_at_utils

    def get_symbol_dividend_info_by_symbol(self, symbol: str) -> SymbolDividendInfo:
        return self._nike_sql_session.query(SymbolDividendInfo) \
            .filter(SymbolDividendInfo.symbol == symbol) \
            .first()

    def dividend_indicator(self, symbol: str) -> dict:
        cache_key = self._dividend_indicator_cache_key(symbol)
        cached = self._cache.get(cache_key)
        if cached:
            return cached

        symbol_info: SymbolInfo = self._symbol_info_service.get_symbol_info_by_symbol(symbol)
        if not symbol_info:
            raise DataNotFound("找不到指定的Symbol")

        updated_at = self._updated_at_utils.get_date("nike", "symbol_dividend_info")

        column = {
            "symbol": symbol,
            "sameIndustry": symbol_info.main_category,
            "allMarket": "ALL_MARKET"
        }

        data = {
            "updatedAt": updated_at
        }

        for c in column:
            dividend_info: SymbolDividendInfo = self.get_symbol_dividend_info_by_symbol(column[c])
            if not dividend_info:
                data[c] = {
                    "dividendYield": None,
                    "filledDays": None,
                    "filledRatio": None,
                    "volatility": None,
                    "continuousYear": None
                }
                continue
            data[c] = {
                "dividendYield": decimal_to_float(dividend_info.dividend_yield),
                "filledDays": decimal_to_float(dividend_info.filled_days),
                "filledRatio": decimal_to_float(dividend_info.filled_ratio),
                "volatility": decimal_to_float(dividend_info.volatility),
                "continuousYear": dividend_info.continuous_year
            }
        self._cache.set(cache_key, data, timeout=3600)
        return data

    @staticmethod
    def _dividend_indicator_cache_key(symbol: str) -> str:
        return f"{symbol}_dividend_indicator"
