VALUE_INDICATOR_MAP = {
    "REVENUE-YOY": ["REVENUE_YOY_MRQ", "REVENUE_YOY_MRT", "REVENUE_YOY_MRY"],
    "NCFO-NETINC": ["NCFO_NETINC_ratio_MRQ", "NCFO_NETINC_ratio_MRT", "NCFO_NETINC_ratio_MRY"],
    "EPS": ["EPS_MRQ", "EPS_MRT", "EPS_MRY"],
    "ROE-ROA-ROS-ROIC": [
        "ROE_MRQ", "ROE_MRT", "ROE_MRY",
        "ROA_MRQ", "ROA_MRT", "ROA_MRY",
        "ROS_MRQ", "ROS_MRT", "ROS_MRY",
        "ROIC_MRQ", "ROIC_MRT", "ROIC_MRY",
    ],
    "PE-RATIO": ["monthly_avg_price"]
}
