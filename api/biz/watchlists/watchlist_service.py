from sqlalchemy.orm import Session

from api.biz.error import InvalidInvocation
from api.biz.watchlists.business_watchlist_service import BusinessWatchlistService
from api.biz.watchlists.normal_watchlist_service import NormalWatchlistService


class WatchlistService:
    def __init__(
            self,
            user_sql_session: Session,
            normal_watchlist_service: NormalWatchlistService,
            business_watchlist_service: BusinessWatchlistService
    ) -> None:
        self._user_sql_session = user_sql_session
        self._normal_watchlist_service = normal_watchlist_service
        self._business_watchlist_service = business_watchlist_service

    def add_watchlist(self, user_id: str, account_type: str, name: str) -> None:
        if account_type == "normal":
            self._normal_watchlist_service.add_watchlist(user_id, name)
        elif account_type == "business":
            self._business_watchlist_service.add_watchlist(user_id, name)
        else:
            raise InvalidInvocation("account_type is invalid.")

    def delete_watchlist(self, user_id: str, account_type: str, watchlist_id: str) -> None:
        if account_type == "normal":
            self._normal_watchlist_service.delete_watchlist(user_id, watchlist_id)
        elif account_type == "business":
            self._business_watchlist_service.delete_watchlist(user_id, watchlist_id)
        else:
            raise InvalidInvocation("account_type is invalid.")

    def rename_watchlist(self, user_id: str, account_type: str, watchlist_id: str, name: str) -> None:
        if account_type == "normal":
            self._normal_watchlist_service.rename_watchlist(user_id, watchlist_id, name)
        elif account_type == "business":
            self._business_watchlist_service.rename_watchlist(user_id, watchlist_id, name)
        else:
            raise InvalidInvocation("account_type is invalid.")

    def add_symbol(self, user_id: str, account_type: str, watchlist_id: str, symbol: str) -> None:
        if account_type == "normal":
            self._normal_watchlist_service.add_symbol(user_id, watchlist_id, symbol)
        elif account_type == "business":
            self._business_watchlist_service.add_symbol(user_id, watchlist_id, symbol)
        else:
            raise InvalidInvocation("account_type is invalid.")

    def remove_symbol(self, user_id: str, account_type: str, watchlist_id: str, symbol: str) -> None:
        if account_type == "normal":
            self._normal_watchlist_service.remove_symbol(user_id, watchlist_id, symbol)
        elif account_type == "business":
            self._business_watchlist_service.remove_symbol(user_id, watchlist_id, symbol)
        else:
            raise InvalidInvocation("account_type is invalid.")
