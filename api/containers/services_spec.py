import paramiko
import pinject
from flask import Flask

from api.biz.backtest.backtest_action_service import BacktestActionService
from api.biz.backtest.backtest_result_service import BacktestResultService
from api.biz.backtest.backtest_trend_service import BacktestTrendService
from api.biz.backtest.cumulative_roi_service import CumulativeRoiService
from api.biz.account.business_account_service import BusinessAccountService
from api.biz.content.content_service import ContentService
from api.biz.historical.historical_dividend_service import HistoricalDividendService
from api.biz.historical.historical_rating_service import HistoricalRatingService
from api.biz.historical.historical_symbol_price_service import HistoricalSymbolPriceService
from api.biz.indicator.dividend_indicator_service import DividendIndicatorService
from api.biz.indicator.indicator_result_service import IndicatorResultService
from api.biz.indicator.trend_indicator_service import TrendIndicatorService
from api.biz.indicator.value_indicator_service import ValueIndicatorService
from api.biz.institution.chip_basic_info_service import ChipBasicInfoService
from api.biz.institution.chip_institution_holding_info_service import ChipInstitutionHoldingInfoService
from api.biz.institution.famous_investor_status_service import FamousInvestorStatusService
from api.biz.institution.insider_buying_info_service import InsiderBuyingInfoService
from api.biz.institution.investor_ranking_service import InvestorRankingService
from api.biz.search_engine.screener_service import ScreenerService
from api.biz.search_engine.search_symbol_service import SearchSymbolService
from api.biz.service_status.stock_mining_service import StockMiningServiceStatus
from api.biz.stripe.cancel_subscribe_record_service import CancelSubscribeRecordService
from api.biz.stripe.stripe_customer_info_service import StripeCustomerInfoService
from api.biz.stripe.stripe_service import StripeService
from api.biz.stripe.stripe_session_info_service import StripeSessionInfoService
from api.biz.stripe.stripe_subscribe_info_service import StripeSubscribeInfoService
from api.biz.stripe.stripe_webhook_service import StripeWebhookService
from api.biz.symbol.symbol_info_service import SymbolInfoService
from api.biz.symbol.symbol_service import SymbolService
from api.biz.account.tradingview_account_service import TradingviewAccountService
from api.biz.account.normal_account_service import NormalAccountService
from api.biz.watchlists.business_watchlist_service import BusinessWatchlistService
from api.biz.watchlists.normal_watchlist_service import NormalWatchlistService
from api.biz.watchlists.watchlist_service import WatchlistService
from api.common.mail.system_mailer import SystemMailer
from api.common.updated_at_utils import UpdatedAtUtils
from config.api_config import Config

service_classes = [
    BacktestActionService,
    BacktestResultService,
    BacktestTrendService,
    BusinessAccountService,
    BusinessWatchlistService,
    CancelSubscribeRecordService,
    ChipBasicInfoService,
    ChipInstitutionHoldingInfoService,
    ContentService,
    CumulativeRoiService,
    DividendIndicatorService,
    FamousInvestorStatusService,
    HistoricalDividendService,
    HistoricalRatingService,
    HistoricalSymbolPriceService,
    IndicatorResultService,
    InsiderBuyingInfoService,
    InvestorRankingService,
    NormalAccountService,
    NormalWatchlistService,
    ScreenerService,
    SearchSymbolService,
    StockMiningServiceStatus,
    StripeCustomerInfoService,
    StripeService,
    StripeSessionInfoService,
    StripeSubscribeInfoService,
    StripeWebhookService,
    StripeWebhookService,
    SymbolInfoService,
    SymbolService,
    SystemMailer,
    TradingviewAccountService,
    TrendIndicatorService,
    UpdatedAtUtils,
    ValueIndicatorService,
    WatchlistService,
]


class BindingSpec(pinject.BindingSpec):
    """Binding setup"""

    def __init__(self, app: Flask):
        self.app = app

    def configure(self, bind):
        bind('app', to_instance=self.app)
        bind('cache', to_instance=self.app.rediscache)
        bind('config', to_instance=Config())
        bind('logger', to_instance=self.app.logger)
        bind('user_sql_session', to_instance=self.app.userdbsession)
        bind('nike_sql_session', to_instance=self.app.nikedbsession)
        bind('symbol_sql_session', to_instance=self.app.symboldbsession)
        bind('sec13f_sql_session', to_instance=self.app.sec13fdbsession)
        bind('nike_mongodb_client', to_instance=self.app.mongodbclient)

    @pinject.provides(in_scope=pinject.SINGLETON)
    def provide_cronjob(self) -> paramiko.SSHClient():
        client = paramiko.SSHClient()
        # add to known hosts
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            client.connect(
                hostname=Config().CRONJOB_HOST, username=Config().CRONJOB_USER, password=Config().CRONJOB_PWD)
        except:
            print("[!] Cannot connect to the SSH Server")
            exit()

        return client


