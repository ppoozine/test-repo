import random


def get_password(passwd_len=8):
    chars = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789"
    passwd = ""
    for x in range(0, passwd_len):
        passwd += random.choice(chars)
    return passwd
