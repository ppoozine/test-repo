from functools import wraps

from flask import current_app
from flask_caching import Cache
from flask_jwt_extended import get_jwt
from flask_jwt_extended import verify_jwt_in_request

from api.biz.account.business_account_service import BusinessAccountService
from api.biz.account.normal_account_service import NormalAccountService
from api.common.mail.system_mailer import SystemMailer
from config.api_config import Config


def jwt_required():
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            verify_jwt_in_request()
            jwt_data = get_jwt()
            user_id = None
            account_type = None
            if "user_id" in jwt_data:
                user_id = get_jwt()["user_id"]
                jwt_cache_key = f"{user_id}_jwt"
            if "exp" in jwt_data:
                exp_timestamp = get_jwt()["exp"]
            if "account_type" in jwt_data:
                account_type = get_jwt()["account_type"]
            if "email" in jwt_data:
                email = get_jwt()["email"]

            cache: Cache = current_app.rediscache
            cached = cache.get(jwt_cache_key)
            if cached:
                if cached == jwt_data:
                    return fn(*args, **kwargs)
                elif exp_timestamp > cached["exp"]:
                    cache.set(jwt_cache_key, jwt_data, timeout=3600)  # update token
                    return fn(*args, **kwargs)
                else:
                    message = {
                        "return_code": -402,
                        "return_msg": "AuthenticationFailed: Token timeout",
                        "return_data": []
                    }
                    return message
            if user_id and account_type in ["normal", "business"]:
                if account_type == "normal":
                    normal_account_service = NormalAccountService(SystemMailer(Config), current_app.userdbsession)
                    if normal_account_service.get_user(user_id=user_id):
                        cache.set(jwt_cache_key, jwt_data, timeout=3600)  # update token
                        return fn(*args, **kwargs)
                    else:
                        normal_account_service.add_user(user_id=user_id, email=email)
                        cache.set(jwt_cache_key, jwt_data, timeout=3600)
                        return fn(*args, **kwargs)
                if account_type == "business":
                    business_account_service = BusinessAccountService(current_app.userdbsession)
                    if business_account_service.get_user_by_user_id(user_id):
                        cache.set(jwt_cache_key, jwt_data, timeout=3600)  # update token
                        return fn(*args, **kwargs)
            else:
                message = {
                    "return_code": -401,
                    "return_msg": "AuthenticationFailed: Is not a valid token.",
                    "return_data": []
                }
                return message

        return decorator

    return wrapper
