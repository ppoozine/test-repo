from flask import Flask
from flask_mongoengine import MongoEngine
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session


class UserDBSession:
    def __init__(self, app=None) -> None:
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app: Flask):
        engine = create_engine(
            app.config["USER_DATABASE_URI"],
            pool_size=app.config["SQLALCHEMY_POOL_SIZE"],
            pool_recycle=app.config["SQLALCHEMY_POOL_RECYCLE"],
            pool_timeout=app.config["SQLALCHEMY_POOL_TIMEOUT"],
            encoding="utf8",
            pool_pre_ping=True
        )

        sessionlocal = sessionmaker(bind=engine, autocommit=True)
        app.userdbsession = scoped_session(sessionlocal)


class NikeDBSession:
    def __init__(self, app=None) -> None:
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app: Flask):
        engine = create_engine(
            app.config["NIKE_DATABASE_URI"],
            pool_size=app.config["SQLALCHEMY_POOL_SIZE"],
            pool_recycle=app.config["SQLALCHEMY_POOL_RECYCLE"],
            pool_timeout=app.config["SQLALCHEMY_POOL_TIMEOUT"],
            encoding="utf8",
            pool_pre_ping=True
        )

        sessionlocal = sessionmaker(bind=engine, autocommit=True)
        app.nikedbsession = scoped_session(sessionlocal)


class SymbolDBSession:
    def __init__(self, app=None) -> None:
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app: Flask):
        engine = create_engine(
            app.config["SYMBOL_DATABASE_URI"],
            pool_size=app.config["SQLALCHEMY_POOL_SIZE"],
            pool_recycle=app.config["SQLALCHEMY_POOL_RECYCLE"],
            pool_timeout=app.config["SQLALCHEMY_POOL_TIMEOUT"],
            encoding="utf8",
            pool_pre_ping=True
        )

        sessionlocal = sessionmaker(bind=engine, autocommit=True)
        app.symboldbsession = scoped_session(sessionlocal)


class SEC13FDBSession:
    def __init__(self, app=None) -> None:
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app: Flask):
        engine = create_engine(
            app.config["SEC13F_DATABASE_URI"],
            pool_size=app.config["SQLALCHEMY_POOL_SIZE"],
            pool_recycle=app.config["SQLALCHEMY_POOL_RECYCLE"],
            pool_timeout=app.config["SQLALCHEMY_POOL_TIMEOUT"],
            encoding="utf8",
            pool_pre_ping=True
        )

        sessionlocal = sessionmaker(bind=engine, autocommit=True)
        app.sec13fdbsession = scoped_session(sessionlocal)


class MongoClientFactory(object):
    def __init__(self, app=None):
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app: Flask):
        app.config["MONGODB_SETTINGS"]
        app.mongodbclient = MongoEngine(app)
