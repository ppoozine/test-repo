from logging import Logger
from typing import Tuple

from flask import current_app
from flask import request
from flask_caching import Cache
from flask_jwt_extended import get_jwt, create_access_token, decode_token
from flask_restful import Resource
from requests import Response

from api.biz.account.business_account_service import BusinessAccountService
from api.biz.error import DataValidationError, TenantDataConflictError
from api.common.jwt_utils import jwt_required
from api.common.response_utils import api_response
from api.containers.services_container import inject_service


class ApiGenerateBusinessAccount(Resource):

    @inject_service()
    def __init__(self, business_account_service: BusinessAccountService) -> None:
        self._business_account_service = business_account_service

    def post(self) -> Response:
        req = request.get_json()
        self._check_payload(req)

        self._business_account_service.generate_business_account(
            req["username"],
            req["password"],
            req["company"],
            req["company_code"],
            req["plan"],
            req["subscribed_at"],
            req["expire_at"]
        )
        return api_response(message="Success")

    @staticmethod
    def _check_payload(req: dict) -> None:
        if not isinstance(req, dict):
            raise DataValidationError("Body not json.")
        for r in ["username", "password", "company", "company_code", "plan", "subscribed_at", "expire_at"]:
            if r not in req:
                raise DataValidationError(f"缺少 {r} 參數.")
            if not isinstance(r, str):
                raise DataValidationError(f"{r} 應為字串型態.")


class ApiBusinessAccountLogin(Resource):

    @inject_service()
    def __init__(self, business_account_service: BusinessAccountService) -> None:
        self._business_account_service = business_account_service

    def post(self) -> Response:
        req = request.get_json()
        self._check_payload(req)

        data = self._business_account_service.get_user(req["user_name"], req["password"])
        if not data:
            message = {
                "return_code": -401,
                "return_msg": "user_name and password are not working!",
            }
            return api_response(message=message)

        access_token = create_access_token(
            identity=req["user_name"],
            additional_claims={
                "user_id": req["user_name"],
                "account_type": "business"
            }
        )

        return api_response(return_data={"token": access_token}, message="Success")

    @staticmethod
    def _check_payload(req: dict) -> None:
        if not isinstance(req, dict):
            raise DataValidationError("Body not json.")
        for r in ["user_name", "password"]:
            if r not in req:
                raise DataValidationError(f"缺少 {r} 參數.")
            if not isinstance(r, str):
                raise DataValidationError(f"{r} 應為字串型態.")


class ApiBusinessAccountLogout(Resource):
    @inject_service()
    def __init__(self, cache: Cache, business_account_service: BusinessAccountService) -> None:
        self._cache = cache
        self._business_account_service = business_account_service

    @jwt_required()
    def post(self) -> Response:
        user_id, account_type = self._check_jwt()
        if not user_id or account_type != "business":
            raise TenantDataConflictError(f"jwt token is invalid.")

        data = self._business_account_service.get_user_by_user_id(user_id)
        if not data:
            return api_response(message="False", http_status=400)

        additional_claims = {
            "user_id": user_id,
            "account_type": "business"
        }
        access_token = create_access_token(identity=user_id, additional_claims=additional_claims)
        jwt_data = decode_token(access_token)
        jwt_cache_key = f"{user_id}_jwt"
        self._cache.set(jwt_cache_key, jwt_data, timeout=3600)  # update token
        return api_response(message="True")

    @staticmethod
    def _check_jwt() -> Tuple[str, str]:
        jwt = get_jwt()
        if "user_id" not in jwt or "account_type" not in jwt:
            raise TenantDataConflictError(f"jwt token is invalid.")

        user_id = jwt["user_id"]
        account_type = jwt["account_type"]
        return user_id, account_type
