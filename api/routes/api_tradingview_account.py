from flask import request
from flask_jwt_extended import get_jwt
from flask_restful import Resource
from requests import Response

from api.biz.error import DataValidationError
from api.biz.account.tradingview_account_service import TradingviewAccountService
from api.common.jwt_utils import jwt_required
from api.common.response_utils import api_response
from api.containers.services_container import inject_service


class ApiTradingviewAccount(Resource):

    @inject_service()
    def __init__(self, tradingview_account_service: TradingviewAccountService) -> None:
        self._tradingview_account_service = tradingview_account_service

    @jwt_required()
    def post(self) -> Response:
        user_id = get_jwt()["user_id"]
        req = request.get_json()
        self._check_payload(req)

        self._tradingview_account_service.account(user_id, req["tradingviewAccount"])
        return api_response(message="Success")

    def _check_payload(self, req: dict) -> None:
        if not isinstance(req, dict):
            raise DataValidationError("Body not json.")

        for r in ["tradingviewAccount"]:
            if r not in req:
                raise DataValidationError(f"缺少 {r} 參數.")
