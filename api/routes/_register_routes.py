from flask import Flask
from flask_restful import Api

from api.routes.api_ai_sector_optional import ApiAiSectorOptional
from api.routes.api_backtest_action import ApiBacktestAction
from api.routes.api_backtest_result import ApiBacktestResult
from api.routes.api_business_account import ApiGenerateBusinessAccount, ApiBusinessAccountLogin, ApiBusinessAccountLogout
from api.routes.api_chip_basic_info import ApiChipBasicInfo
from api.routes.api_chip_institution_holding_info import ApiChipInstitutionHoldingInfo
from api.routes.api_content import ApiContent
from api.routes.api_cumulative_roi import ApiCumulativeRoi
from api.routes.api_default_search_symbol import ApiDefaultSearchSymbol
from api.routes.api_famous_investor_status import ApiFamousInvestorStatus
from api.routes.api_historical_rating import ApiHistoricalRating
from api.routes.api_home import ApiHome
from api.routes.api_insider_buying_info import ApiInsiderBuyingInfo
from api.routes.api_investor_ranking import ApiInvestorRanking
from api.routes.api_related_symbol import ApiRelatedSymbol
from api.routes.api_screener import ApiScreener
from api.routes.api_screener_option import ApiScreenerOption
from api.routes.api_search_symbol import ApiSearchSymbol
from api.routes.api_stock_mining_service_status import ApiStockMiningServiceStatus
from api.routes.api_subscribe_plan import ApiSubscribePlan
from api.routes.api_symbol_dividend_indicator import ApiSymbolDividendIndicator
from api.routes.api_symbol_historical_dividend import ApiSymbolHistoricalDividend
from api.routes.api_symbol_info import ApiSymbolInfo
from api.routes.api_symbol_price import ApiSymbolPrice
from api.routes.api_symbol_trend_indicator import ApiSymbolTrendIndicator
from api.routes.api_symbol_value_indicator import ApiSymbolValueIndicator
from api.routes.api_symbols_score import ApiSymbolsScore
from api.routes.api_tradingview_account import ApiTradingviewAccount
from api.routes.api_trend_performance import ApiTrendPerformance
from api.routes.api_user_profile import ApiUserProfile
from api.routes.api_watchlist import ApiWatchlist, ApiWatchlistRename
from api.routes.api_watchlist import ApiWatchlistAppendSymbol, ApiWatchlistRemoveSymbol
from api.routes.open_api_spec import OpenApiSpec
from api.routes.stripe.api_stripe_customer_portal import ApiStripeCustomerPortal
from api.routes.stripe.api_stripe_customer_portal_update_page import ApiStripeCustomerPortalUpdatePage
from api.routes.stripe.api_stripe_webhook import ApiStripeWebhook
from config.api_config import Config


def setup_routes(app: Flask):
    if Config.ENV_SET.upper() in ["LOCAL", "DEV", "SIT"]:
        @app.route('/open-api-spec', defaults={'path': "index.html"})
        @app.route('/open-api-spec/', defaults={'path': "index.html"})
        @app.route('/open-api-spec/<path>')
        def send_open_api_spec(path: str):
            return OpenApiSpec.resolve_content(path)

    api = Api(app)
    api.add_resource(ApiHome, "/")

    # 個股探勘服務狀態
    api.add_resource(ApiStockMiningServiceStatus, "/stock-mining-service/<service>")

    # 官網
    api.add_resource(ApiAiSectorOptional, "/official-website/ai-sector-optional/<oriented>")

    # User 相關
    api.add_resource(ApiUserProfile, "/users", methods=['GET'])
    api.add_resource(ApiTradingviewAccount, "/tradingview/account")

    # Business User
    api.add_resource(ApiBusinessAccountLogin, "/business_login")
    api.add_resource(ApiBusinessAccountLogout, "/business_logout")
    api.add_resource(ApiGenerateBusinessAccount, "/add_business_users", methods=['POST'])

    # Watchlist 相關
    api.add_resource(ApiWatchlist, "/watchlists", endpoint="watchlist_post", methods=['POST'])
    api.add_resource(ApiWatchlist, "/watchlists/<watchlist_id>", methods=['DELETE'])
    api.add_resource(ApiWatchlistRename, "/watchlists/<watchlist_id>/rename")
    api.add_resource(ApiWatchlistAppendSymbol, "/watchlists/<watchlist_id>/append")
    api.add_resource(ApiWatchlistRemoveSymbol, "/watchlists/<watchlist_id>/remove")

    # 搜尋
    api.add_resource(ApiSearchSymbol, "/search/symbol/<keyword>")
    api.add_resource(ApiDefaultSearchSymbol, "/default/search/symbol")

    # Symbol
    api.add_resource(ApiSymbolInfo, "/symbol-info/<symbol>")
    api.add_resource(ApiRelatedSymbol, "/related-symbol/<symbol>")
    api.add_resource(ApiSymbolsScore, "/symbol/score")

    # 篩選器
    api.add_resource(ApiScreenerOption, "/screener/option")
    api.add_resource(ApiScreener, "/screener")

    # 回測
    api.add_resource(ApiBacktestAction, "/backtest-action/<symbol>/<oriented>/<strategy>/<start_at>/<end_at>")
    api.add_resource(ApiBacktestResult, "/backtest-result/<symbol>/<oriented>/<strategy>")
    api.add_resource(ApiTrendPerformance, "/trend-performance/<symbol>")
    api.add_resource(ApiCumulativeRoi, "/cumulative-roi/<symbol>/<oriented>/<strategy>/<end_at>")

    # 機構
    api.add_resource(ApiChipBasicInfo, "/chip-basic-info/<symbol>")
    api.add_resource(ApiChipInstitutionHoldingInfo, "/chip-institution-holding-info/<symbol>")
    api.add_resource(ApiInsiderBuyingInfo, "/insider-buying-info/<symbol>/<page>/<page_size>")
    api.add_resource(ApiInvestorRanking, "/investor-ranking/<symbol>/<page>/<page_size>")
    api.add_resource(ApiFamousInvestorStatus, "/famous-investor-status/<symbol>")

    # 文案
    api.add_resource(ApiContent, "/content/<symbol>/<oriented>/<info>")

    # 指標
    api.add_resource(ApiSymbolDividendIndicator, "/dividend-indicator/<symbol>")
    api.add_resource(ApiSymbolTrendIndicator, "/trend-indicator/<symbol>")
    api.add_resource(ApiSymbolValueIndicator, "/value-indicator/<symbol>/<indicator>")

    # 歷史資料
    api.add_resource(ApiHistoricalRating, "/historical-rating/<symbol>/<oriented>")
    api.add_resource(ApiSymbolHistoricalDividend, "/historical-dividend/<symbol>/<nums>")
    api.add_resource(ApiSymbolPrice, "/symbol/price/<symbol>/<start_at>/<end_at>")

    # Stripe 相關
    api.add_resource(ApiSubscribePlan, "/subscribe/plan")
    api.add_resource(ApiStripeWebhook, "/stripe/webhook")
    api.add_resource(ApiStripeCustomerPortal, "/stripe/customer-portal")
    api.add_resource(ApiStripeCustomerPortalUpdatePage, "/stripe/customer-portal-update-page")
