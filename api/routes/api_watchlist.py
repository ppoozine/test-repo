from typing import Tuple

from flask import request
from flask_jwt_extended import get_jwt
from flask_restful import Resource
from requests import Response

from api.biz.error import DataValidationError, TenantDataConflictError
from api.biz.watchlists.watchlist_service import WatchlistService
from api.common.jwt_utils import jwt_required
from api.common.response_utils import api_response
from api.containers.services_container import inject_service


class ApiWatchlist(Resource):
    @inject_service()
    def __init__(self, watchlist_service: WatchlistService) -> None:
        self._watchlist_service = watchlist_service

    @jwt_required()
    def post(self) -> Response:
        user_id, account_type = self._check_jwt()

        req = request.get_json()
        self._check_payload(req)

        self._watchlist_service.add_watchlist(user_id, account_type, req["name"])
        return api_response(message="Success")

    @jwt_required()
    def delete(self, watchlist_id: str) -> Response:
        user_id, account_type = self._check_jwt()

        self._watchlist_service.delete_watchlist(user_id, account_type, watchlist_id)
        return api_response(message="Success")

    @staticmethod
    def _check_jwt() -> Tuple[str, str]:
        jwt = get_jwt()
        if "user_id" not in jwt or "account_type" not in jwt:
            raise TenantDataConflictError(f"jwt token is invalid.")

        user_id = jwt["user_id"]
        account_type = jwt["account_type"]
        return user_id, account_type

    @staticmethod
    def _check_payload(req: dict) -> None:
        if not isinstance(req, dict):
            raise DataValidationError("Body not json.")
        for r in ["name"]:
            if r not in req:
                raise DataValidationError(f"缺少 {r} 參數.")
        if not isinstance(req["name"], str):
            raise DataValidationError("name 應為字串型態.")


class ApiWatchlistRename(Resource):
    @inject_service()
    def __init__(self, watchlist_service: WatchlistService) -> None:
        self._watchlist_service = watchlist_service

    @jwt_required()
    def post(self, watchlist_id: str) -> Response:
        user_id, account_type = self._check_jwt()

        req = request.get_json()
        self._check_payload(req)

        self._watchlist_service.rename_watchlist(user_id, account_type, watchlist_id, req["name"])
        return api_response(message="Success")

    @staticmethod
    def _check_jwt() -> Tuple[str, str]:
        jwt = get_jwt()
        if "user_id" not in jwt or "account_type" not in jwt:
            raise TenantDataConflictError(f"jwt token is invalid.")

        user_id = jwt["user_id"]
        account_type = jwt["account_type"]
        return user_id, account_type

    @staticmethod
    def _check_payload(req: dict) -> None:
        if not isinstance(req, dict):
            raise DataValidationError("Body not json.")
        for r in ["name"]:
            if r not in req:
                raise DataValidationError(f"缺少 {r} 參數.")
        if not isinstance(req["name"], str):
            raise DataValidationError("name 應為字串型態.")


class ApiWatchlistAppendSymbol(Resource):
    @inject_service()
    def __init__(self, watchlist_service: WatchlistService) -> None:
        self._watchlist_service = watchlist_service

    @jwt_required()
    def post(self, watchlist_id: str) -> Response:
        user_id, account_type = self._check_jwt()

        req = request.get_json()
        self._check_payload(req)

        self._watchlist_service.add_symbol(user_id, account_type, watchlist_id, req["symbol"])
        return api_response(message="Success")

    @staticmethod
    def _check_jwt() -> Tuple[str, str]:
        jwt = get_jwt()
        if "user_id" not in jwt or "account_type" not in jwt:
            raise TenantDataConflictError(f"jwt token is invalid.")

        user_id = jwt["user_id"]
        account_type = jwt["account_type"]
        return user_id, account_type

    @staticmethod
    def _check_payload(req: dict) -> None:
        if not isinstance(req, dict):
            raise DataValidationError("Body not json.")
        for r in ["symbol"]:
            if r not in req:
                raise DataValidationError(f"缺少 {r} 參數.")
        if not isinstance(req["symbol"], str):
            raise DataValidationError("symbol 應為字串型態.")


class ApiWatchlistRemoveSymbol(Resource):
    @inject_service()
    def __init__(self, watchlist_service: WatchlistService) -> None:
        self._watchlist_service = watchlist_service

    @jwt_required()
    def post(self, watchlist_id: str) -> Response:
        user_id, account_type = self._check_jwt()

        req = request.get_json()
        self._check_payload(req)

        self._watchlist_service.remove_symbol(user_id, account_type, watchlist_id, req["symbol"])
        return api_response(message="Success")

    @staticmethod
    def _check_jwt() -> Tuple[str, str]:
        jwt = get_jwt()
        if "user_id" not in jwt or "account_type" not in jwt:
            raise TenantDataConflictError(f"jwt token is invalid.")

        user_id = jwt["user_id"]
        account_type = jwt["account_type"]
        return user_id, account_type

    @staticmethod
    def _check_payload(req: dict) -> None:
        if not isinstance(req, dict):
            raise DataValidationError("Body not json.")
        for r in ["symbol"]:
            if r not in req:
                raise DataValidationError(f"缺少 {r} 參數.")
        if not isinstance(req["symbol"], str):
            raise DataValidationError("symbol 應為字串型態.")
