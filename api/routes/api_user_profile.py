from typing import Tuple

from flask_jwt_extended import get_jwt
from flask_restful import Resource
from requests import Response

from api.biz.account.business_account_service import BusinessAccountService
from api.biz.account.normal_account_service import NormalAccountService
from api.biz.error import TenantDataConflictError, InvalidInvocation
from api.common.jwt_utils import jwt_required
from api.common.response_utils import api_response
from api.containers.services_container import inject_service


class ApiUserProfile(Resource):

    @inject_service()
    def __init__(
            self,
            normal_account_service: NormalAccountService,
            business_account_service: BusinessAccountService,
    ) -> None:
        self._business_account_service = business_account_service
        self._normal_account_service = normal_account_service

    @jwt_required()
    def get(self) -> Response:
        user_id, account_type = self._check_jwt()
        if account_type == "normal":
            data = self._normal_account_service.get_user(user_id)
            data.update({"account_type": account_type})
        elif account_type == "business":
            data = self._business_account_service.get_user_by_user_id(user_id)
            data.update({"account_type": account_type})
        else:
            raise InvalidInvocation("account_type is invalid.")

        return api_response(data=data, message="Success")

    @staticmethod
    def _check_jwt() -> Tuple[str, str]:
        jwt = get_jwt()
        if "user_id" not in jwt or "account_type" not in jwt:
            raise TenantDataConflictError(f"jwt token is invalid.")

        user_id = jwt["user_id"]
        account_type = jwt["account_type"]
        return user_id, account_type
