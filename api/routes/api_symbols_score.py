from flask import request
from flask_restful import Resource
from requests import Response

from api.biz.error import DataValidationError
from api.biz.symbol.symbol_info_service import SymbolInfoService
from api.common.jwt_utils import jwt_required
from api.common.response_utils import api_response
from api.containers.services_container import inject_service


class ApiSymbolsScore(Resource):

    @inject_service()
    def __init__(self, symbol_info_service: SymbolInfoService) -> None:
        self._symbol_info_service = symbol_info_service

    @jwt_required()
    def post(self) -> Response:
        req = request.get_json()
        self._check_payload(req)

        data = self._symbol_info_service.symbol_list_score(req["symbolList"])
        return api_response(data=data, message="Success")

    @staticmethod
    def _check_payload(req: dict) -> None:
        if not isinstance(req, dict):
            raise DataValidationError("Body not json.")

        if "symbolList" not in req:
            raise DataValidationError("缺少 symbolList 參數")
