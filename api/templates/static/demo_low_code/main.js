console.log("Stripe check!");

// Get Stripe publishable key
fetch("/stripe/config")
    .then((result) => {
        return result.json();
    })
    .then((data) => {
        // Initialize Stripe.js
        const stripe = Stripe(data.data.publishKey);

        // Event handler
        document.getElementById("submitBtn1").addEventListener("click", executeApi)
        document.getElementById("submitBtn2").addEventListener("click", executeApi)
        document.getElementById("submitBtn3").addEventListener("click", executeApi)
        document.getElementById("submitBtn4").addEventListener("click", executeApi)
        document.getElementById("submitBtn5").addEventListener("click", executeApi)
        document.getElementById("submitBtn6").addEventListener("click", executeApi)
        document.getElementById("submitBtn7").addEventListener("click", executeApi)

        function executeApi(event) {
            if (event.target.id === "submitBtn1") {
                body = JSON.stringify({item: 1})
            } else if (event.target.id === "submitBtn2") {
                body = JSON.stringify({item: 2})
            } else if (event.target.id === "submitBtn3") {
                body = JSON.stringify({item: 3})
            } else if (event.target.id === "submitBtn4") {
                body = JSON.stringify({item: 4})
            } else if (event.target.id === "submitBtn5") {
                body = JSON.stringify({item: 5})
            } else if (event.target.id === "submitBtn6") {
                body = JSON.stringify({item: 6})
            } else if (event.target.id === "submitBtn7") {
                body = JSON.stringify({item: 7})
            }
            fetch("/stripe/demo/low-code/checkout-session", {
                method: "POST",
                headers: {"Content-Type": "application/json"},
                body
            })
                .then((result) => {
                    return result.json();
                })
                .then((data) => {
                    console.log(data);
                    // Redirect to Stripe Checkout
                    return stripe.redirectToCheckout({sessionId: data.data.sessionId})
                })
                .then((res) => {
                    console.log(res);
                });

        }

    });