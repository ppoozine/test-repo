db = new Mongo().getDB("user_profile");
db.createCollection("user_profile");
db.createCollection("business_account")

db.user_profile.insertMany([
    {
        "user_id": "account-test",
        "email": "test@email.com",
        "plan": "",
        "subscribed": false,
        "subscribed_at": "",
        "subscribed_expired_at": "",
        "use_trial": false,
        "trial_end_at": "",
        "watchlists": "",
        "updated_at": "",
        "created_at": "2022-10-10 00:00:00"
    }
])