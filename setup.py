import setuptools

setuptools.setup(
    name='nike',
    version='1.0.0',
    description='description of this project',
    author='morrison',
    author_email=['morrison@leadinfo.com.tw', 'amoswang@leadinfo.com.tw'],
    scripts=["scripts/nike"],
    install_requires=['fire', 'spock_app_base'],
    packages=setuptools.find_packages(),
    license='MIT',
    url='http://pypi.leadinfo.com.tw/',
    python_requires='>=3.10',
    zip_safe=False,
    include_package_data=True
)
