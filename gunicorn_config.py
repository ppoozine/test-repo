import multiprocessing as mp
bind = "0.0.0.0:5000"
workers = mp.cpu_count() * 2


# gunicorn -c gunicorn_config.py --capture-output --log-level info mainapp:app
